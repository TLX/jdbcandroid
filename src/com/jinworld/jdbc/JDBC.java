package com.jinworld.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class JDBC extends Activity {
	TextView tv;
	TextView details;
	int count;
	private static final String TAG = "JDBC.jinin";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		tv = (TextView) findViewById(R.id.tv);
		details = (TextView) findViewById(R.id.details);

		new ConnectivityAsyncTask().execute();
	}

	private class ConnectivityAsyncTask extends AsyncTask<Void, Integer, Integer> {
		protected Integer doInBackground(Void... x) {
			Connection conn = null;
			Integer status = 1;
			try {
				String db_user = "jin";
				String db_pass = "lin";
				String db_url = "jdbc:mysql://192.168.1.4/world?connectTimeout=5000"; // 192.168.1.4, 10.0.2.2
				Class.forName("com.mysql.jdbc.Driver").newInstance();
				Log.w(TAG, "starting connection attempt");
				// DriverManager.setLoginTimeout(5); //doesnt work so use Connector/J db_url implementation
				conn = DriverManager.getConnection(db_url, db_user, db_pass);
				Log.w(TAG, "connection established");

				Statement s = conn.createStatement();
				s.executeQuery("SELECT * FROM world.city;");
				ResultSet rs = s.getResultSet();
				count = 0;
				while (rs.next()) {
					// int id = rs.getInt("ID");
					// String uname = rs.getString("Name");
					// int age = rs.getInt("age");
					// System.out.println(id+"\t"+uname+"\t"+age);
					// System.out.println(uname);
					count++;
					Log.w(TAG, "cycle: " + count);
				}
				rs.close();
				s.close();

			} catch (SQLException e) {
				Log.e(TAG, e.toString());
				publishProgress(96);
				return -1;
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return -2;
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return -3;
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return -4;
			} finally {
				if (conn != null) {
					try {
						Log.v(TAG, "closed conn in finally");
						publishProgress(999);
						status = 0;
						conn.close();
					} catch (Exception e) {
						publishProgress(69);
						Log.e(TAG, e.toString());
						return -5;
					}
				}
			}
			return status;
		}

		protected void onProgressUpdate(Integer... stepNum) {
			switch (stepNum[0]) {
			case 1:
				details.setText("something went wrong");
				break;
			case 999:
				details.setText("error: EVERYTHING IS OK");
				break;
			case 96:
				details.setText("EXCEPTION IN TRY");
				break;
			case 69:
				details.setText("EXCEPTION IN FINALLY");
				break;
			default:
				details.setText("unknown error");
			}
		}

		protected void onPostExecute(Integer status) {
			switch (status) {
			case 0:
				tv.setText("WE ARE DONE " + Integer.toString(count));
				break;
			case 1:
				tv.setText("TASK DID NOT COMPLETE");
				break;
			default:
				tv.setText("ERROR NUM: "+status.toString());
			}
		}
	}

}